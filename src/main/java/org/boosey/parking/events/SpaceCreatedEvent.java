package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class SpaceCreatedEvent extends EventBase<SpaceCreatedEvent> {
    public String spaceId;
    public String number;

    @Incoming("space-created-incoming")
    public void notify(String e){
        this.notifySubscribers(e, this.getClass());
    }

    public static SpaceCreatedEvent with(ObjectId spaceId, String number) {
        SpaceCreatedEvent  e = new SpaceCreatedEvent();
        e.channelName = "space-created";
        e.spaceId = getObjectIdAsString(spaceId);
        e.number = number;
        return e;
    }

    @Outgoing("space-created")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("space-created");
   }
}