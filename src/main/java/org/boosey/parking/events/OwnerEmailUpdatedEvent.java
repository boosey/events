package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class OwnerEmailUpdatedEvent extends EventBase<OwnerEmailUpdatedEvent> {
    public String ownerId;
    public String email;

    @Incoming("owner-email-updated-incoming")
    public void notify(String e){
        this.notifySubscribers(e, this.getClass());
    }

    public static OwnerEmailUpdatedEvent with(ObjectId ownerId, String email) {
        OwnerEmailUpdatedEvent e = new OwnerEmailUpdatedEvent();
        e.channelName = "owner-email-updated";
        e.ownerId = getObjectIdAsString(ownerId);
        e.email = email;
        return e;
    }

    @Outgoing("owner-email-updated")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("owner-email-updated");
   }
}