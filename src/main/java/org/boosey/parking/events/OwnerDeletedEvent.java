package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@Slf4j
@ApplicationScoped
public class OwnerDeletedEvent extends EventBase<OwnerDeletedEvent> {
    public String ownerId;


    @Incoming("owner-deleted-incoming")
    public void notify(String e){
        log.info("OwnerDELETED notify");
        this.notifySubscribers(e, this.getClass());
    }

    public static OwnerDeletedEvent with(ObjectId ownerId) {
        OwnerDeletedEvent e = new OwnerDeletedEvent();
        e.channelName = "owner-deleted";
        e.ownerId = getObjectIdAsString(ownerId);

        return e;
    }

    @Outgoing("owner-deleted")
    public Flowable<String> createStreamEmitter() {
        log.info("Creating owner-deleted");
        return this.createStream("owner-deleted");
   }
}
