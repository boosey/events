package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@Slf4j
@ApplicationScoped
public class SpaceResidentRemovedEvent extends EventBase<SpaceResidentRemovedEvent> {
    public String spaceId;

    @Incoming("space-resident-removed-incoming")
    public void notify(String e){
        log.info("Space Resident Removed notify");
        this.notifySubscribers(e, this.getClass());
    }

    public static SpaceResidentRemovedEvent with(ObjectId spaceId) {
        SpaceResidentRemovedEvent e = new SpaceResidentRemovedEvent();
        e.channelName = "space-resident-removed";
        e.spaceId = getObjectIdAsString(spaceId);

        return e;
    }

    @Outgoing("space-resident-removed")
    public Flowable<String> createStreamEmitter() {
        log.info("Creating space-resident-removed");
        return this.createStream("space-resident-removed");
   }
}
