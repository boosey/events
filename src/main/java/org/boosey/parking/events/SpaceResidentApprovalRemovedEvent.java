package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@Slf4j
@ApplicationScoped
public class SpaceResidentApprovalRemovedEvent extends EventBase<SpaceResidentApprovalRemovedEvent> {
    public String spaceId;

    @Incoming("space-resident-approval-removed-incoming")
    public void notify(String e){
        log.info("Space Resident Approval Removed notify");
        this.notifySubscribers(e, this.getClass());
    }

    public static SpaceResidentApprovalRemovedEvent with(ObjectId spaceId) {
        SpaceResidentApprovalRemovedEvent e = new SpaceResidentApprovalRemovedEvent();
        e.channelName = "space-resident-approval-removed";
        e.spaceId = getObjectIdAsString(spaceId);

        return e;
    }

    @Outgoing("space-resident-approval-removed")
    public Flowable<String> createStreamEmitter() {
        log.info("Creating space-resident-approval-removed");
        return this.createStream("space-resident-approval-removed");
   }
}
